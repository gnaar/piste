import logging
from unittest.mock import Mock
from importlib import import_module, reload

import pytest
from gnar_gear import GnarApp

from piste.test.constants import ENVIRONMENT

log = logging.getLogger()


class TestApis:

    @pytest.fixture
    def mock_app(self, monkeypatch):
        monkeypatch.setattr('argparse.ArgumentParser.parse_args',
                            lambda *a, **kw: type('args', (object,), {'port': ''}))
        monkeypatch.setattr('flask.Flask.run', lambda *a, **kw: None)
        monkeypatch.setattr('os.environ', ENVIRONMENT)
        monkeypatch.setattr('postgres.Postgres.__init__', lambda *a, **kw: None)
        mock_client = type('sqs_client', (object,), {'get_queue_url': lambda *a, **kw: {'QueueUrl': 'Canada'},
                                                     'receive_message': Mock(return_value={})})
        monkeypatch.setattr('boto3.Session.client', lambda *a, **kw: mock_client)
        monkeypatch.setattr('redis.StrictRedis',
                            lambda *a, **kw: type('redis_connection', (object,), {'get': lambda *a: None}))
        main = import_module('piste.app.main')
        reload(main)
        app = GnarApp('piste', production=False, port=9400)
        app.run()
        yield app.flask.test_client()

    def test_10_45(self, monkeypatch, mock_app):
        mock = Mock(return_value=type('response', (object,), {'json': lambda: 'Off-Piste checking in!'}))
        with monkeypatch.context() as m:
            m.setattr('requests.get', mock)
            mock_app.get('/10-45')
            mock.assert_called_with('http://127.0.0.1:9401/check-in', None)

    def test_get_message(self, monkeypatch, mock_app):
        response = mock_app.get('/10-45/get-message/xxx-xxx-xxxxxxxx-xxx')
        assert response.json == 'Sorry, no message for you right now.'
        with monkeypatch.context() as m:
            m.setattr('redis.StrictRedis',
                      lambda *a, **kw: type('redis_connection', (object,), {'get': lambda *a: b'Exit Stage Left'}))
            main = import_module('piste.app.main')
            reload(main)
            mock_app = GnarApp('piste', production=False, port=9400)
            mock_app.run()
            response = mock_app.flask.test_client().get('/10-45/get-message/xxx-xxx-xxxxxxxx-xxx')
            assert response.json == 'Exit Stage Left'
