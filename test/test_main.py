from importlib import reload
from runpy import _run_module_as_main
from unittest.mock import Mock, patch

import gnar_gear

from piste.test.constants import ENVIRONMENT


class TestMain:

    def test_init(self, monkeypatch):
        monkeypatch.setattr('os.environ', ENVIRONMENT)
        monkeypatch.setattr('psycopg2.connect', lambda *a, **kw: None)
        monkeypatch.setattr('argparse.ArgumentParser.parse_args', lambda x: type('args', (object,), {'port': ''}))
        monkeypatch.setattr('bjoern._default_instance',
                            (type('mock_sock', (object,), {'family': None, 'close': lambda: None}), None))
        monkeypatch.setattr('bjoern.server_run', lambda *a, **kw: None)
        messages = [{'Messages': [{'MessageId': 1, 'Body': "All the World's a Stage", 'ReceiptHandle': 'xxx'}]}, {}]
        mock_client = type('sqs_client', (object,), {'get_queue_url': lambda *a, **kw: {'QueueUrl': 'Canada'},
                                                     'receive_message': Mock(side_effect=messages),
                                                     'delete_message': Mock()})
        monkeypatch.setattr('boto3.Session.client', lambda *a, **kw: mock_client)
        mock_redis_set = Mock()
        monkeypatch.setattr('redis.StrictRedis',
                            lambda *a, **kw: type('redis_connection', (object,), {'set': mock_redis_set}))
        with patch('bjoern.listen') as mock_run:
            reload(gnar_gear)
            _run_module_as_main('piste.app.main')
            assert mock_run.call_count == 1

            mock_redis_set.assert_called_with(1, "All the World's a Stage", 3600)
